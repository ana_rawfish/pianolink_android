package it.rawfish.pianolink;


import android.app.Application;

import com.facebook.FacebookSdk;

import net.danlew.android.joda.JodaTimeAndroid;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class PianolinkApp extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
				.setDefaultFontPath("fonts/pier_regular.otf")
				.setFontAttrId(R.attr.fontPath)
				.build()
		);

		FacebookSdk.sdkInitialize(this);

		JodaTimeAndroid.init(this);
	}
}
