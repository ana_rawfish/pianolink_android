package it.rawfish.pianolink.models;


public class Avatar {
	long id;
	String original;
	String s;
	String m;
	String l;

	public Avatar(long id, String original, String s, String m, String l) {
		this.id = id;
		this.original = original;
		this.s = s;
		this.m = m;
		this.l = l;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public String getM() {
		return m;
	}

	public void setM(String m) {
		this.m = m;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}
}
