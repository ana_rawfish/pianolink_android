package it.rawfish.pianolink.models;


import java.util.List;

public class DataFeed {
	List<ItemFeed> data;

	public DataFeed(List<ItemFeed> data) {
		this.data = data;
	}

	public List<ItemFeed> getData() {
		return data;
	}

	public void setData(List<ItemFeed> data) {
		this.data = data;
	}
}
