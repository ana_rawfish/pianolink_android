package it.rawfish.pianolink.models;


import java.util.ArrayList;

public class ItemFeed {
	long id;

	int rating;
	int user_rating;

	String title;
	String content_type;
	String instrument;
	String preview_image;
	String level;
	String created_at;

	boolean bookmarked;
	boolean private_content;

	ArrayList<String> music_genres;
	ArrayList<String> media_types;

	User user;

	public ItemFeed(long id, int rating, int user_rating, String title, String content_type, String instrument, String preview_image, String level, String created_at, boolean bookmarked, boolean private_content, ArrayList<String> music_genres, ArrayList<String> media_types, User user) {
		this.id = id;
		this.rating = rating;
		this.user_rating = user_rating;
		this.title = title;
		this.content_type = content_type;
		this.instrument = instrument;
		this.preview_image = preview_image;
		this.level = level;
		this.created_at = created_at;
		this.bookmarked = bookmarked;
		this.private_content = private_content;
		this.music_genres = music_genres;
		this.media_types = media_types;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getUser_rating() {
		return user_rating;
	}

	public void setUser_rating(int user_rating) {
		this.user_rating = user_rating;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent_type() {
		return content_type;
	}

	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getPreview_image() {
		return preview_image;
	}

	public void setPreview_image(String preview_image) {
		this.preview_image = preview_image;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public boolean isBookmarked() {
		return bookmarked;
	}

	public void setBookmarked(boolean bookmarked) {
		this.bookmarked = bookmarked;
	}

	public ArrayList<String> getMusic_genres() {
		return music_genres;
	}

	public void setMusic_genres(ArrayList<String> music_genres) {
		this.music_genres = music_genres;
	}

	public ArrayList<String> getMedia_types() {
		return media_types;
	}

	public void setMedia_types(ArrayList<String> media_types) {
		this.media_types = media_types;
	}

	public boolean isPrivate_content() {
		return private_content;
	}

	public void setPrivate_content(boolean private_content) {
		this.private_content = private_content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
