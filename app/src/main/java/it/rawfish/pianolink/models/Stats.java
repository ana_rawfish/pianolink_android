
package it.rawfish.pianolink.models;


public class Stats {

    public long questions_count;
    public long lessons_count;
    public long tutorials_count;

    public Stats() {
    }

    /**
     * 
     * @param tutorials_count
     * @param lessons_count
     * @param questions_count
     */
    public Stats(long questions_count, long lessons_count, long tutorials_count) {
        super();
        this.questions_count = questions_count;
        this.lessons_count = lessons_count;
        this.tutorials_count = tutorials_count;
    }

    public long getQuestionsCount() {
        return questions_count;
    }

    public void setQuestionsCount(long questionsCount) {
        this.questions_count = questionsCount;
    }

    public long getLessonsCount() {
        return lessons_count;
    }

    public void setLessonsCount(long lessonsCount) {
        this.lessons_count = lessonsCount;
    }

    public long getTutorialsCount() {
        return tutorials_count;
    }

    public void setTutorialsCount(long tutorialsCount) {
        this.tutorials_count = tutorialsCount;
    }
}
