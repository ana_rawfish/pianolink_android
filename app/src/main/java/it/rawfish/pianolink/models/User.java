package it.rawfish.pianolink.models;


public class User {
	long id;

	String nickname;
	String avatar;

	boolean bookmarked;

	public User(long id, String nickname, String avatar, boolean bookmarked) {
		this.id = id;
		this.nickname = nickname;
		this.avatar = avatar;
		this.bookmarked = bookmarked;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public boolean isBookmarked() {
		return bookmarked;
	}

	public void setBookmarked(boolean bookmarked) {
		this.bookmarked = bookmarked;
	}
}
