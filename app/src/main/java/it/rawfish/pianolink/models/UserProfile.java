
package it.rawfish.pianolink.models;

import java.util.List;

import it.rawfish.pianolink.utils.Constants;

public class UserProfile {

	public long id;
	public long user_id;
	public long ask_filter_min_age;
	public long ask_filter_max_age;

	public String nickname;
	public String email;
	public String name;
	public String surname;
	public String phone;
	public String gender;
	public String dob;
	public String job;
	public String app_language;
	public String address;
	public String city;
	public String nation;
	public String spoken_language;
	public String website;
	public String musician_type;
	public String instrument;
	public String bio;
	public String user_options;
	public String ask_filter_city;
	public String created_at;
	public String updated_at;

	public Stats stats;
	public Avatar avatar;

	public List<String> ask_filter_music_types = null;
	public List<String> ask_filter_musician_types = null;
	public List<String> images = null;
	public List<String> music_types = null;

	public boolean is_completed;
	public boolean has_password;

	/**
	 * No args constructor for use in serialization
	 */
	public UserProfile() {
	}

	/**
	 * @param id
	 * @param user_id
	 * @param nickname
	 * @param email
	 * @param name
	 * @param surname
	 * @param phone
	 * @param gender
	 * @param dob
	 * @param job
	 * @param app_language
	 * @param address
	 * @param city
	 * @param nation
	 * @param spoken_language
	 * @param website
	 * @param musician_type
	 * @param music_types
	 * @param instrument
	 * @param avatar
	 * @param images
	 * @param bio
	 * @param user_options
	 * @param stats
	 * @param is_completed
	 * @param ask_filter_city
	 * @param ask_filter_min_age
	 * @param ask_filter_max_age
	 * @param ask_filter_music_types
	 * @param ask_filter_musician_types
	 * @param has_password
	 * @param created_at
	 * @param updated_at
	 */
	public UserProfile(long id, long user_id, String nickname, String email, String name, String surname, String phone, String gender, String dob, String job, String app_language, String address, String city, String nation, String spoken_language, String website, String musician_type, List<String> music_types, String instrument, Avatar avatar, List<String> images, String bio, String user_options, Stats stats, boolean is_completed, String ask_filter_city, long ask_filter_min_age, long ask_filter_max_age, List<String> ask_filter_music_types, List<String> ask_filter_musician_types, boolean has_password, String created_at, String updated_at) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.nickname = nickname;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.phone = phone;
		this.gender = gender;
		this.dob = dob;
		this.job = job;
		this.app_language = app_language;
		this.address = address;
		this.city = city;
		this.nation = nation;
		this.spoken_language = spoken_language;
		this.website = website;
		this.musician_type = musician_type;
		this.music_types = music_types;
		this.instrument = instrument;
		this.avatar = avatar;
		this.images = images;
		this.bio = bio;
		this.user_options = user_options;
		this.stats = stats;
		this.is_completed = is_completed;
		this.ask_filter_city = ask_filter_city;
		this.ask_filter_min_age = ask_filter_min_age;
		this.ask_filter_max_age = ask_filter_max_age;
		this.ask_filter_music_types = ask_filter_music_types;
		this.ask_filter_musician_types = ask_filter_musician_types;
		this.has_password = has_password;
		this.created_at = created_at;
		this.updated_at = updated_at;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return user_id;
	}

	public void setUserId(long userId) {
		this.user_id = userId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getAppLanguage() {
		return app_language;
	}

	public void setAppLanguage(String appLanguage) {
		this.app_language = appLanguage;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getSpokenLanguage() {
		return spoken_language;
	}

	public void setSpokenLanguage(String spokenLanguage) {
		this.spoken_language = spokenLanguage;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getMusicianType() {
		return musician_type;
	}

	public void setMusicianType(String musicianType) {
		this.musician_type = musicianType;
	}

	public List<String> getMusicTypes() {
		return music_types;
	}

	public void setMusicTypes(List<String> musicTypes) {
		this.music_types = musicTypes;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public Avatar getAvatar() {
		return avatar;
	}

	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getUser_options() {
		return user_options;
	}

	public void setUser_options(String user_options) {
		this.user_options = user_options;
	}

	public Stats getStats() {
		return stats;
	}

	public void setStats(Stats stats) {
		this.stats = stats;
	}

	public boolean isIsCompleted() {
		return is_completed;
	}

	public void setIsCompleted(boolean isCompleted) {
		this.is_completed = isCompleted;
	}

	public String getAskFilterCity() {
		return ask_filter_city;
	}

	public void setAskFilterCity(String askFilterCity) {
		this.ask_filter_city = askFilterCity;
	}

	public long getAskFilterMinAge() {
		return ask_filter_min_age;
	}

	public void setAskFilterMinAge(long askFilterMinAge) {
		this.ask_filter_min_age = askFilterMinAge;
	}

	public long getAskFilterMaxAge() {
		return ask_filter_max_age;
	}

	public void setAskFilterMaxAge(long askFilterMaxAge) {
		this.ask_filter_max_age = askFilterMaxAge;
	}

	public List<String> getAskFilterMusicTypes() {
		return ask_filter_music_types;
	}

	public void setAskFilterMusicTypes(List<String> askFilterMusicTypes) {
		this.ask_filter_music_types = askFilterMusicTypes;
	}

	public List<String> getAsk_filter_musician_types() {
		return ask_filter_musician_types;
	}

	public void setAsk_filter_musician_types(List<String> ask_filter_musician_types) {
		this.ask_filter_musician_types = ask_filter_musician_types;
	}

	public boolean isHasPassword() {
		return has_password;
	}

	public void setHasPassword(boolean hasPassword) {
		this.has_password = hasPassword;
	}

	public String getCreatedAt() {
		return created_at;
	}

	public void setCreatedAt(String createdAt) {
		this.created_at = createdAt;
	}

	public String getUpdatedAt() {
		return updated_at;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updated_at = updatedAt;
	}

	public boolean isProfessional() {
		if (musician_type == null)
			return false;

		return musician_type.equals(Constants.PROFESSIONAL);
	}

	public boolean isMusicType(String typeToCheck) {
		if (music_types == null)
			return false;

		for (String type : music_types) {
			if (type.equals(typeToCheck))
				return true;
		}

		return false;
	}
}
