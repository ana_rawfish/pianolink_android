
package it.rawfish.pianolink.models;


public class UserProfileData {

    private UserProfile data;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserProfileData() {
    }

    /**
     * 
     * @param data
     */
    public UserProfileData(UserProfile data) {
        super();
        this.data = data;
    }

    public UserProfile getData() {
        return data;
    }

    public void setData(UserProfile data) {
        this.data = data;
    }

}
