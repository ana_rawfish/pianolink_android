package it.rawfish.pianolink.rest;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RestBaseCallBack<T>  implements Callback<T> {

	public RestBaseCallBack() {
	}

	@Override
	public void onResponse(Call call, Response response) {
		if (response.isSuccessful()) {
			onSuccess((T)response.body());
		} else {
			try {
				onError(response.errorBody().string());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onFailure(Call call, Throwable t) {
		onError(t.getMessage());
	}

	public abstract void onSuccess(T obj);
	public abstract void onError(String error);
}
