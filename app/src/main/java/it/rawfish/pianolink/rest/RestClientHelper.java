package it.rawfish.pianolink.rest;

public class RestClientHelper {

    public interface RequestCallback {
        void onSuccess(Object obj);
        void onError();
    }
}
