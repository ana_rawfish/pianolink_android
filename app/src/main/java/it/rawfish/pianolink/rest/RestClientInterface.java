package it.rawfish.pianolink.rest;

import it.rawfish.pianolink.models.DataFeed;
import it.rawfish.pianolink.models.UserProfileData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestClientInterface {
	@GET("contents")
	Call<DataFeed> getHomeFeed(@Query("page") int page,
	                           @Query("page_size") int pageSize,
	                           @Query("content_types") String contentTypes);

	@GET("profile")
	Call<UserProfileData> getUserProfile();
}
