package it.rawfish.pianolink.ui.activities;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseAppCompatActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	public void replaceFragment(Fragment fragment, int layoutId) {
		if (!isFinishing()) {
			getSupportFragmentManager()
					.beginTransaction()
					.replace(layoutId, fragment)
					.commitAllowingStateLoss();
		}
	}

	public void replaceNavFragment(Fragment fragment, int layoutId) {
		if (!isFinishing()) {
			getSupportFragmentManager()
					.beginTransaction()
					.replace(layoutId, fragment)
					.addToBackStack(null)
					.commitAllowingStateLoss();
		}
	}
}
