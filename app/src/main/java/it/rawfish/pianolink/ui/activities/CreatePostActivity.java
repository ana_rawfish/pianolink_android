package it.rawfish.pianolink.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import it.rawfish.pianolink.R;
import it.rawfish.pianolink.databinding.ActivityCreatePostBinding;
import it.rawfish.pianolink.ui.fragments.create_post.PostBodyFragment;

public class CreatePostActivity extends BaseAppCompatActivity {
	private ActivityCreatePostBinding mBinding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinding = DataBindingUtil.setContentView(this,
				R.layout.activity_create_post);

		setSupportActionBar(mBinding.toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		replaceFragment(new PostBodyFragment(), R.id.fragment_container);
	}
}
