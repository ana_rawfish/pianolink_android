package it.rawfish.pianolink.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import it.rawfish.pianolink.R;
import it.rawfish.pianolink.databinding.ActivityMainBinding;
import it.rawfish.pianolink.ui.fragments.FeedFragment;
import it.rawfish.pianolink.ui.fragments.HomeFragment;
import it.rawfish.pianolink.ui.fragments.ProfileFragment;
import it.rawfish.pianolink.ui.fragments.SearchFragment;

public class MainActivity extends BaseAppCompatActivity implements AHBottomNavigation.OnTabSelectedListener {

	private ActivityMainBinding mBinding;
	private boolean isMyFeedMenuSelected = false;
	private boolean isSearchShown = false;
	private LinearLayout mUserFeedBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinding = DataBindingUtil.setContentView(this,
				R.layout.activity_main);

		setSupportActionBar(mBinding.toolbar);
		getSupportActionBar().setTitle("");

		setBottomBarItems();

		mBinding.bottomBar.setOnTabSelectedListener(this);

		replaceFragment(new HomeFragment(), R.id.fragment_container);
	}

	@Override
	public void onBackPressed() {
		if (isMyFeedMenuSelected) {
			selectUserFeed(false);
			super.onBackPressed();
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem actionViewItem = menu.findItem(R.id.action_feed);
		MenuItem searchItem = menu.findItem(R.id.action_search);
		View v = MenuItemCompat.getActionView(actionViewItem);
		mUserFeedBtn = (LinearLayout) v.findViewById(R.id.notif_btn);
		mUserFeedBtn.setSelected(isMyFeedMenuSelected);

		searchItem.setVisible(isSearchShown);
		actionViewItem.setVisible(!isSearchShown);

		return super.onPrepareOptionsMenu(menu);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		menu.findItem(R.id.action_feed).getActionView().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isMyFeedMenuSelected) {
					selectUserFeed(true);
				}
			}
		});

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void selectUserFeed(boolean isSelected) {
		isMyFeedMenuSelected = isSelected;
		mUserFeedBtn.setSelected(isMyFeedMenuSelected);
		getSupportActionBar().setDisplayHomeAsUpEnabled(isMyFeedMenuSelected);

		if (isMyFeedMenuSelected) {
			mBinding.logo.setVisibility(View.GONE);
			getSupportActionBar().setTitle("My feed");
			replaceNavFragment(new FeedFragment(), R.id.fragment_container);
		} else {
			mBinding.logo.setVisibility(View.VISIBLE);
			getSupportActionBar().setTitle("");
		}
	}

	private void setBottomBarItems() {
		AHBottomNavigationItem item1 = new AHBottomNavigationItem(null, R.drawable.ic_home);
		AHBottomNavigationItem item2 = new AHBottomNavigationItem(null, R.drawable.ic_home);
		AHBottomNavigationItem item3 = new AHBottomNavigationItem(null, R.drawable.ic_home);
		AHBottomNavigationItem item4 = new AHBottomNavigationItem(null, R.drawable.ic_home);
		AHBottomNavigationItem item5 = new AHBottomNavigationItem(null, R.drawable.ic_home);

		mBinding.bottomBar.addItem(item1);
		mBinding.bottomBar.addItem(item2);
		mBinding.bottomBar.addItem(item3);
		mBinding.bottomBar.addItem(item4);
		mBinding.bottomBar.addItem(item5);

		mBinding.bottomBar.setAccentColor(Color.BLACK);
		mBinding.bottomBar.setDefaultBackgroundColor(getResources().getColor(R.color.colorPrimary));
		mBinding.bottomBar.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
	}

	@Override
	public boolean onTabSelected(int position, boolean wasSelected) {
		switch (position) {
			case 0:
				replaceFragment(new HomeFragment(), R.id.fragment_container);
				break;
			case 1:
				replaceFragment(new SearchFragment(), R.id.fragment_container);
				break;
			case 2:
				Intent intent = new Intent(MainActivity.this, CreatePostActivity.class);
				startActivity(intent);
				break;
			case 3:
				break;
			case 4:
				replaceFragment(new ProfileFragment(), R.id.fragment_container);
				break;
		}

		isSearchShown = position == 1;
		invalidateOptionsMenu();

		return true;
	}
}
