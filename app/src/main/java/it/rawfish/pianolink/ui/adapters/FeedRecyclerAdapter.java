package it.rawfish.pianolink.ui.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.swipe.BaseSwipeAdapter;
import com.malinskiy.superrecyclerview.swipe.SwipeLayout;

import java.util.List;

import it.rawfish.pianolink.databinding.ItemSwipableBinding;
import it.rawfish.pianolink.models.ItemFeed;

public class FeedRecyclerAdapter extends BaseSwipeAdapter<FeedRecyclerAdapter.ViewHolder> {

	private List<ItemFeed> mItems;

	public FeedRecyclerAdapter(List<ItemFeed> mItems) {
		this.mItems = mItems;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemSwipableBinding binding = ItemSwipableBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

		return new ViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		super.onBindViewHolder(holder, position);

		ItemSwipableBinding binding = holder.mBinding;
		binding.feedItem.setItem(mItems.get(position));

		SwipeLayout swipeLayout = holder.swipeLayout;
		swipeLayout.setDragEdge(SwipeLayout.DragEdge.Left);
		swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

		swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
			@Override
			public void onDoubleClick(SwipeLayout layout, boolean surface) {
			}
		});

		binding.delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(holder.getAdapterPosition());
			}
		});
	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}

	public void add(ItemFeed item) {
		insert(item, mItems.size());
	}

	public void insert(ItemFeed item, int position) {
		closeAllExcept(null);

		mItems.add(position, item);

		notifyItemInserted(position);
	}

	public void remove(int position) {
		mItems.remove(position);

		closeItem(position);

		notifyItemRemoved(position);
	}

	public static class ViewHolder extends BaseSwipeAdapter.BaseSwipeableViewHolder {
		ItemSwipableBinding mBinding;

		public ViewHolder(ItemSwipableBinding binding) {
			super(binding.getRoot());
			mBinding = binding;
		}
	}
}
