package it.rawfish.pianolink.ui.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import it.rawfish.pianolink.databinding.ItemFeedBinding;
import it.rawfish.pianolink.models.ItemFeed;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
	private List<ItemFeed> mItems;

	public HomeRecyclerAdapter(List<ItemFeed> mItems) {
		this.mItems = mItems;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemFeedBinding binding = ItemFeedBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new ViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		ItemFeedBinding binding = ((ViewHolder) holder).mBinding;
		binding.setItem(mItems.get(position));
	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}

	public void addItems(List<ItemFeed> items) {
		mItems.addAll(items);
		notifyDataSetChanged();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public ItemFeedBinding mBinding;

		public ViewHolder(ItemFeedBinding binding) {
			super(binding.getRoot());
			mBinding = binding;
		}
	}

	public interface FeedListListener {
	}
}
