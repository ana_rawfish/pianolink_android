package it.rawfish.pianolink.ui.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import it.rawfish.pianolink.databinding.SimpleDropdownItemBinding;
import it.rawfish.pianolink.models.Tag;

public class TagsAutocompleteArrayAdapter extends ArrayAdapter<Tag> {
	private List<Tag> mTags;

	public TagsAutocompleteArrayAdapter(Context context, int resource, List<Tag> objects) {
		super(context, resource, objects);
		mTags = objects;
	}

	@Override
	public int getCount() {
		return mTags.size();
	}

	@Override
	public Tag getItem(int i) {
		return mTags.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@NonNull
	@Override
	public View getView(int i, View convertView, @NonNull ViewGroup viewGroup) {
		final PartViewHolder holder;

		if (convertView == null) {
			SimpleDropdownItemBinding binding = SimpleDropdownItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
			holder = new PartViewHolder(binding);
			convertView = holder.mBinding.getRoot();
			convertView.setTag(holder);
		} else {
			holder = (PartViewHolder) convertView.getTag();
		}

		holder.mBinding.text.setText(mTags.get(i).getName());

		return convertView;
	}

	private class PartViewHolder {
		SimpleDropdownItemBinding mBinding;

		PartViewHolder(SimpleDropdownItemBinding mBinding) {
			this.mBinding = mBinding;
		}
	}

	@NonNull
	@Override
	public Filter getFilter() {
		return mNameFilter;
	}

	private Filter mNameFilter = new Filter() {
		@Override
		public String convertResultToString(Object resultValue) {
			return ((Tag)(resultValue)).getName();
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			if(constraint != null) {
				ArrayList<Tag> suggestions = new ArrayList<>();
				for (Tag part : mTags) {
					if(part.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
						suggestions.add(part);
					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = suggestions;
				filterResults.count = suggestions.size();
				return filterResults;
			} else {
				return new FilterResults();
			}
		}
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			if(results != null && results.count > 0) {
				notifyDataSetChanged();
			}
		}
	};
}
