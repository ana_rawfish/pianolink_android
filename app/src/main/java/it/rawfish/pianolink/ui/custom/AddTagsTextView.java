package it.rawfish.pianolink.ui.custom;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.tokenautocomplete.TokenCompleteTextView;

import it.rawfish.pianolink.databinding.TagViewBinding;
import it.rawfish.pianolink.models.Tag;

public class AddTagsTextView extends TokenCompleteTextView<Tag> {

	public AddTagsTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AddTagsTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public AddTagsTextView(Context context) {
		super(context);
	}

	@Override
	protected View getViewForObject(Tag object) {
		TagViewBinding binding = TagViewBinding.inflate(LayoutInflater.from(getContext()), null, false);
		binding.tagName.setText(object.getName());
		return binding.getRoot();
	}

	@Override
	protected Tag defaultObject(String completionText) {
		return new Tag(completionText);
	}
}
