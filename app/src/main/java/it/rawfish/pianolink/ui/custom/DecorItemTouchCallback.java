package it.rawfish.pianolink.ui.custom;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

public class DecorItemTouchCallback extends ItemTouchHelper.SimpleCallback {
	// we want to cache these and not allocate anything repeatedly in the onChildDraw method
	private Drawable mDrawable;
	private final int mDrawableMargin = 60;
	private boolean isInitiated;
	private DecorRecycleViewAdapter mAdapter;

	public DecorItemTouchCallback(Drawable drawable, DecorRecycleViewAdapter adapter, int dragDirs, int swipeDirs) {
		super(dragDirs, swipeDirs);

		mDrawable = drawable;
		mAdapter = adapter;
	}

	private void init() {
		mDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
		isInitiated = true;
	}

	// not important, we don't want drag & drop
	@Override
	public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
		return false;
	}

	@Override
	public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
		int position = viewHolder.getAdapterPosition();
		DecorRecycleViewAdapter testAdapter = (DecorRecycleViewAdapter)recyclerView.getAdapter();
		if (testAdapter.isUndoOn() && testAdapter.isPendingRemoval(position)) {
			return 0;
		}
		return super.getSwipeDirs(recyclerView, viewHolder);
	}

	@Override
	public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
		int swipedPosition = viewHolder.getAdapterPosition();
		if (mAdapter != null) {
			boolean undoOn = mAdapter.isUndoOn();
			if (undoOn) {
				mAdapter.pendingRemoval(swipedPosition);
			} else {
				mAdapter.remove(swipedPosition);
			}
		}
	}

	@Override
	public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
		View itemView = viewHolder.itemView;

		// not sure why, but this method get's called for viewholder that are already swiped away
		if (viewHolder.getAdapterPosition() == -1) {
			// not interested in those
			return;
		}

		if (!isInitiated) {
			init();
		}

		// draw x mark
		int itemHeight = itemView.getBottom() - itemView.getTop();
		int intrinsicWidth = mDrawable.getIntrinsicWidth();
		int intrinsicHeight = mDrawable.getIntrinsicHeight();

		int xMarkLeft = itemView.getLeft() + mDrawableMargin;
		int xMarkRight = intrinsicWidth + mDrawableMargin;
		int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2 + mDrawableMargin;
		int xMarkBottom = xMarkTop + intrinsicHeight;
		mDrawable.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

		mDrawable.draw(c);

		super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
	}
}
