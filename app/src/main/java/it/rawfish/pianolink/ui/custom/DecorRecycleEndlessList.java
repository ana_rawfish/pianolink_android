package it.rawfish.pianolink.ui.custom;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;

public class DecorRecycleEndlessList extends RecyclerView {
	private int mCurrentPage = 0;
	private int mItemsVisibleThreshold = 5;
	private int mLastVisibleItem, mTotalItemCount;
	private boolean isEndlessLoading = true;
	private EndlessListListener mListener;

	public DecorRecycleEndlessList(Context context) {
		super(context);
		init();
	}

	public DecorRecycleEndlessList(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DecorRecycleEndlessList(Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public void init() {
		setLayoutManager(new LinearLayoutManager(getContext()));
		addOnScrollListener(mOnScrollLisener);
	}

	public void setSwipeToDelete(Drawable deleteIcon, boolean isUndo) {
		setHasFixedSize(true);

		if (getAdapter() instanceof DecorRecycleViewAdapter) {
			DecorRecycleViewAdapter adapter = (DecorRecycleViewAdapter) getAdapter();
			adapter.setUndoOn(isUndo);
			ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(new DecorItemTouchCallback(
					deleteIcon,
					adapter,
					0, ItemTouchHelper.RIGHT
			));

			mItemTouchHelper.attachToRecyclerView(this);
			addItemDecoration(new DecorItemDecoration());
		}
	}

	public void setEndlessRecycleViewListener(EndlessListListener listener) {
		mListener = listener;
	}

	private OnScrollListener mOnScrollLisener = new OnScrollListener() {
		@Override
		public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
			super.onScrolled(recyclerView, dx, dy);

			LinearLayoutManager linearLayoutManager = (LinearLayoutManager) getLayoutManager();

			mTotalItemCount = linearLayoutManager.getItemCount();
			mLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
			if(isEndlessLoading && mTotalItemCount <= (mLastVisibleItem + mItemsVisibleThreshold)){
				++mCurrentPage;
				if (mListener != null)
					mListener.onFetchPage(mCurrentPage);
				isEndlessLoading = false;
			}
		}
	};

	public interface EndlessListListener {
		void onFetchPage(int page);
	}
}
