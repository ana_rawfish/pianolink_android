package it.rawfish.pianolink.ui.fragments;


import it.rawfish.pianolink.models.DataFeed;
import it.rawfish.pianolink.rest.RestBaseCallBack;
import it.rawfish.pianolink.rest.RestClient;
import it.rawfish.pianolink.ui.adapters.HomeRecyclerAdapter;
import it.rawfish.pianolink.ui.fragments.base.ListBaseArgFragment;
import it.rawfish.pianolink.utils.Constants;

public class HomeFragment extends ListBaseArgFragment {

	private HomeRecyclerAdapter mHomeAdapter;

	public HomeFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCustomizeView() {
		fetchHomeFeedData(getCurrentPage());
	}

	@Override
	public void onFetchMoreData(int page) {
		fetchHomeFeedData(page);
	}

	@Override
	public void onTabClicked(int index, String title) {

	}

	private void fetchHomeFeedData(int page) {
		RestClient.get().getHomeFeed(page, Constants.LIST_ITEMS_PER_PAGE, Constants.CONTENT_ALL).enqueue(new RestBaseCallBack<DataFeed>() {
			@Override
			public void onSuccess(DataFeed obj) {
				if (mHomeAdapter == null) {
					mHomeAdapter = new HomeRecyclerAdapter(obj.getData());
					applyAdapter(mHomeAdapter);
				} else {
					mHomeAdapter.addItems(obj.getData());
				}
			}

			@Override
			public void onError(String error) {
				//Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
			}
		});
	}
}
