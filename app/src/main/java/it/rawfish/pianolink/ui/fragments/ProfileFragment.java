package it.rawfish.pianolink.ui.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.rawfish.pianolink.databinding.FragmentProfileBinding;
import it.rawfish.pianolink.models.UserProfileData;
import it.rawfish.pianolink.rest.RestBaseCallBack;
import it.rawfish.pianolink.rest.RestClient;
import it.rawfish.pianolink.ui.fragments.base.BaseArgFragment;

public class ProfileFragment extends BaseArgFragment {

	private FragmentProfileBinding mBinding;

	public ProfileFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		mBinding = FragmentProfileBinding.inflate(inflater);

		getProfileInfo();

		return mBinding.getRoot();
	}

	private void getProfileInfo() {
		RestClient.get().getUserProfile().enqueue(new RestBaseCallBack<UserProfileData>() {
			@Override
			public void onSuccess(UserProfileData obj) {
				mBinding.setProfile(obj.getData());
				setTabs();
			}

			@Override
			public void onError(String error) {

			}
		});
	}

	private void setTabs() {
		mBinding.tabs.addTab(mBinding.tabs.newTab().setText(
				String.valueOf(mBinding.getProfile().getStats().getLessonsCount()) + "\n" + "lessons"));
		mBinding.tabs.addTab(mBinding.tabs.newTab().setText(
				String.valueOf(mBinding.getProfile().getStats().getQuestionsCount()) + "\n" + "questions"));
		mBinding.tabs.addTab(mBinding.tabs.newTab().setText(
				String.valueOf(mBinding.getProfile().getStats().getTutorialsCount()) + "\n" + "tutorials"));

		mBinding.tabs.setScrollX(mBinding.tabs.getWidth());
		mBinding.tabs.getTabAt(1).select();

		mBinding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});
	}
}
