package it.rawfish.pianolink.ui.fragments;


import java.util.ArrayList;

import it.rawfish.pianolink.ui.fragments.base.ListBaseArgFragment;

public class SearchFragment extends ListBaseArgFragment {

	public SearchFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCustomizeView() {
		ArrayList<String> tabs = new ArrayList<>(3);
		tabs.add("Tutto");
		tabs.add("Tags");
		tabs.add("Utenti");

		setTabs(tabs);
	}

	@Override
	public void onFetchMoreData(int page) {

	}

	@Override
	public void onTabClicked(int index, String title) {

	}
}
