package it.rawfish.pianolink.ui.fragments.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.lang.reflect.Field;


@FragmentWithArgs
public class BaseArgFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentArgs.inject(this);
	}

	private static final Field sChildFragmentManagerField;

	static {
		Field f = null;
		try {
			f = Fragment.class.getDeclaredField("mChildFragmentManager");
			f.setAccessible(true);
		} catch (NoSuchFieldException e) {
			Log.e("BaseArgFragment", "Error getting mChildFragmentManager field", e);
		}
		sChildFragmentManagerField = f;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		if (sChildFragmentManagerField != null) {
			try {
				sChildFragmentManagerField.set(this, null);
			} catch (Exception e) {
				Log.e("BaseArgFragment", "Error setting mChildFragmentManager field", e);
			}
		}
	}

	public void showToast(String error) {
		if (getActivity() != null) {
			Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
		}
	}
}
