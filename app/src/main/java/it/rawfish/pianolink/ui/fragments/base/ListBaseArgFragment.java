package it.rawfish.pianolink.ui.fragments.base;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.OnMoreListener;

import java.util.ArrayList;

import it.rawfish.pianolink.databinding.FragmentListBinding;
import it.rawfish.pianolink.utils.Constants;

public abstract class ListBaseArgFragment extends BaseArgFragment {
	private FragmentListBinding mBinding;

	private int mActiveTab = 0;
	private String mActiveTabTitle = "";
	private int mCurrentPage = 1;


	public ListBaseArgFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentListBinding.inflate(inflater);

		mBinding.list.setLayoutManager(new LinearLayoutManager(getActivity()));
		mBinding.list.setupMoreListener(new OnMoreListener() {
			@Override
			public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
				++mCurrentPage;
				onFetchMoreData(mCurrentPage);
			}}, Constants.LIST_ITEMS_THRESHOLD);

		onCustomizeView();

		return mBinding.getRoot();
	}

	public FragmentListBinding getBinding() {
		return mBinding;
	}

	public void setTabs(ArrayList<String> tabs) {
		if (getActivity() != null) {
			mBinding.tabs.setVisibility(View.VISIBLE);
			mBinding.tabs.setTabMode(TabLayout.MODE_FIXED);

			for (int i = 0; i < tabs.size(); ++i) {
				mBinding.tabs.addTab(mBinding.tabs.newTab().setText(tabs.get(i)).setTag(i));

				TextView tv = (TextView)(((LinearLayout)
						((LinearLayout)mBinding.tabs.getChildAt(0)).getChildAt(i)).getChildAt(1));
				tv.setTypeface( Typeface.createFromAsset(getActivity().getAssets(), "fonts/pier_regular.otf"));
			}

			mBinding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
				@Override
				public void onTabSelected(TabLayout.Tab tab) {
					mActiveTab = (int)tab.getTag();
					mActiveTabTitle = tab.getText().toString();
					onTabClicked((int)tab.getTag(), tab.getText().toString());
				}

				@Override
				public void onTabUnselected(TabLayout.Tab tab) {

				}

				@Override
				public void onTabReselected(TabLayout.Tab tab) {

				}
			});

			updateTabSelection();
		}
	}

	void updateTabSelection() {
		if (mActiveTab != 0) {
			mBinding.tabs.setScrollX(mBinding.tabs.getWidth());
			mBinding.tabs.getTabAt(mActiveTab).select();
		}

		mActiveTabTitle = mBinding.tabs.getTabAt(mActiveTab).getText().toString();
	}

	public void applyAdapter(RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
		if (getActivity() != null && getBinding() != null) {
			getBinding().list.setAdapter(adapter);
		}
	}

	public int getActiveTab() {
		return mActiveTab;
	}

	public String getActiveTabTitle() {
		return mActiveTabTitle;
	}

	public int getCurrentPage() {
		return mCurrentPage;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mBinding = null;
	}

	public abstract void onCustomizeView();
	public abstract void onFetchMoreData(int page);
	public abstract void onTabClicked(int index, String title);
}
