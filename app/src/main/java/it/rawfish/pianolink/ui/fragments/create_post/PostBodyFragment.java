package it.rawfish.pianolink.ui.fragments.create_post;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.ArrayList;
import java.util.List;

import it.rawfish.pianolink.R;
import it.rawfish.pianolink.databinding.FragmentPostBodyBinding;
import it.rawfish.pianolink.models.Tag;
import it.rawfish.pianolink.ui.adapters.TagsAutocompleteArrayAdapter;

public class PostBodyFragment extends Fragment {

	private FragmentPostBodyBinding mBinding;

	public PostBodyFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		mBinding = FragmentPostBodyBinding.inflate(inflater);

		setBottomBarItems();
		setTagsField();
		return mBinding.getRoot();
	}

	private void setTagsField() {

		List<Tag> tags = new ArrayList<>();
		tags.add(new Tag("spoon"));
		tags.add(new Tag("horse"));
		tags.add(new Tag("bambaleeyooo"));
		tags.add(new Tag("kiyaaa"));
		tags.add(new Tag("parkourkid"));

		TagsAutocompleteArrayAdapter adapter = new TagsAutocompleteArrayAdapter(getActivity(), R.layout.simple_dropdown_item, tags);
		mBinding.tags.setAdapter(adapter);
	}

	private void setBottomBarItems() {
		AHBottomNavigationItem item1 = new AHBottomNavigationItem("link", R.drawable.ic_amateur);
		AHBottomNavigationItem item2 = new AHBottomNavigationItem("video", R.drawable.ic_amateur);
		AHBottomNavigationItem item3 = new AHBottomNavigationItem("audio", R.drawable.ic_amateur);
		AHBottomNavigationItem item4 = new AHBottomNavigationItem("photo", R.drawable.ic_amateur);
		AHBottomNavigationItem item5 = new AHBottomNavigationItem("pdf", R.drawable.ic_amateur);

		mBinding.bottomBar.addItem(item1);
		mBinding.bottomBar.addItem(item2);
		mBinding.bottomBar.addItem(item3);
		mBinding.bottomBar.addItem(item4);
		mBinding.bottomBar.addItem(item5);

		mBinding.bottomBar.setAccentColor(Color.BLACK);
		mBinding.bottomBar.setDefaultBackgroundColor(getResources().getColor(R.color.colorPrimary));
		mBinding.bottomBar.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
	}
}
