package it.rawfish.pianolink.utils;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.ISODateTimeFormat;

import java.text.DecimalFormat;

import it.rawfish.pianolink.R;

public class Binder {
	private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	@BindingAdapter(value = "remoteImage")
	public static void bindRemoteImage(ImageView view, String url) {
		if (url != null && !url.isEmpty()) {
			Picasso.with(view.getContext()).load(url).into(view);
		}
	}

	@BindingAdapter(value = "avatarImage")
	public static void bindAvatarImage(ImageView view, String url) {
		Picasso.with(view.getContext()).load(url).placeholder(R.drawable.ic_avatar_ph_m).into(view);
	}

	@BindingAdapter(value = "distanceValue")
	public static void bindDistanceValue(TextView view, String distance) {
		if (distance != null) {
			DecimalFormat df = new DecimalFormat("#.##");
			String value = df.format(Double.valueOf(distance));
			view.setText(value + "\nkm");
		} else {
			view.setVisibility(View.GONE);
		}
	}

	@BindingAdapter(value = "distanceOneStringValue")
	public static void bindDistanceOneStringValue(TextView view, String distance) {
		String value;
		if (distance != null) {
			DecimalFormat df = new DecimalFormat("#.##");
			value = df.format(Double.valueOf(distance));
			view.setText(value + " km");
		}
	}

	@BindingAdapter(value = "timeDiffValue")
	public static void bindTimeDifference(TextView view, String created) {
		if (created != null) {
			DateTime time = ISODateTimeFormat.dateTime().parseDateTime(created);
			Period p = new Period(time, new DateTime(), PeriodType.dayTime());
			String days = p.getDays() == 0 ? "" : String.valueOf(p.getDays());
			String hours = p.getHours() == 0 ? "" : String.valueOf(p.getHours());
			String minutes = p.getMinutes() == 0 ? "" : String.valueOf(p.getMinutes());
			String seconds = p.getSeconds() == 0 ? "" : String.valueOf(p.getSeconds());

			if (days.isEmpty()) {
				view.setText(view.getContext().getString(R.string.hours_ago, hours, minutes));
			} else if (days.isEmpty() && hours.isEmpty() && !minutes.isEmpty()) {
				view.setText(view.getContext().getString(R.string.minutes_ago, minutes, seconds));
			} else if (days.isEmpty() && hours.isEmpty() && minutes.isEmpty()) {
				view.setText(view.getContext().getString(R.string.seconds_ago, seconds));
			} else if (!days.isEmpty()) {
				view.setText(view.getContext().getString(R.string.days_ago, days));
			}
		}
	}
}
