package it.rawfish.pianolink.utils;


public class Constants {
	public static final String BACK_STACK_ROOT_TAG = "root_fragment";

	public static final String PREFERENCES_NAME = "pianolink_prefs";
    public static final String PREF_INNER_TOKEN = "access_token";
    public static final String PREF_REFRESH_TOKEN = "refresh_token";

	public static final String PACKAGE_TW = "com.twitter.android";
	public static final String PACKAGE_INSTA = "com.instagram.android";
	public static final String PACKAGE_GPLUS = "com.google.android.apps.plus";

	public static final String CONTENT_TUTORIAL= "tutorial";
	public static final String CONTENT_LESSON = "lesson";
	public static final String CONTENT_QUESTION = "question";
	public static final String CONTENT_ALL = CONTENT_TUTORIAL + "," + CONTENT_LESSON + "," + CONTENT_QUESTION;

	public static final String PROFESSIONAL = "professional";
	public static final String MODERN = "modern";
	public static final String CLASSICAL = "classical";

    public static final long PROGRESS_DELAY = 2000;

	public static final int LIST_ITEMS_PER_PAGE = 10;
	public static final int LIST_ITEMS_THRESHOLD = 7;
	public static final int REQUIRED_PASSWORD_LENGHT = 6;
	public static final int SUCCESS_RESULT = 1;
	public static final int FAILURE_RESULT = -1;

	public static final String PACKAGE_NAME =
			"it.rawfish.pianolink";
}
